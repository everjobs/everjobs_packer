# Installing nginx
apt-get -y install nginx

# Installing PHP
apt-get -y install php5

#Installing MySQL
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

apt-get install -y mysql-client mysql-server

#Installing various Uttilites
apt-get install -y git zip unzip curl wget
apt-get install -y php5-mysql php5-memcached php5-xdebug php5-curl php5-json

#Adding VBoxGuestAdditions
sudo ln -s /opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions /usr/lib/VBoxGuestAdditions
